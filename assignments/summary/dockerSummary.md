**Docker**- program to developand run applications

In simple terms it is similar to an ide.
Like ide provides conprehensive facilities
Docker also provides platform for developing,shipping,running
![docker basic](https://d1jnx9ba8s6j9r.cloudfront.net/blog/wp-content/uploads/2016/10/Docker-Client-What-Is-Docker-Container-Edureka.png)
 
### Docker provides tooling and a platform to manage the lifecycle of your containers:

   * Develop your application and its supporting components using containers.
   * The container becomes the unit for distributing and testing your application.
   * When you’re ready, deploy your application into your production environment, as a container
     or an orchestrated service. This works the same whether your production environment is a
     local data center, a cloud provider, or a hybrid of the two.

### Docker Engine is a client-server application with these major components:

* A server which is a type of long-running program called a daemon process (the dockerd command).
* A REST API which specifies interfaces that programs can use to talk to the daemon and instruct
  it what to do.
* A command line interface (CLI) client (the docker command).
![](https://docs.docker.com/engine/images/engine-components-flow.png)

### Docker can be used by users for -
* Fast, consistent delivery of user applications
* Responsive deployment and scaling
* Running more workloads on the same hardware
   ![Docker architecture](https://docs.docker.com/engine/images/architecture.svg)

### What is **Git**?
* Git is a type of _version control system_ (VCS)
* It makes cooridinating work within a team easier by
	* tracking progress
	* tracking changes,edits
* When a change is done ot tells 
	* what exactly is changes 
	* who changes it 
	* why the change was implemented
* The difference between Git and SVN version control systems is-
    Git is a distributed version control system, whereas SVN is a centralized version control
    system. 
![This is how Git works](https://hackbrightacademy.com/content/uploads/2013/08/git_work_flow.png)
* Most of the operation in Git is in the local file 

![This is how other vcs work](https://hackbrightacademy.com/content/uploads/2013/08/svn_work_flow.png)
 
### Git basics terminologies
![Git terminology](https://qph.fs.quoracdn.net/main-qimg-80f0658e8fee461f8806d1d8da807b8a)
* Repository-is a collection of files of various versions of a project or many projects 
* pull- is a command to fetch and download contents from a remote repository to the local
  repository
* push- is a command to upload the local repository to the remote repository.
* commit -is a command to save the changes to the local repository.
 
### Three stages 
1. Modified/update -that means you have changes the contents of the file but not commited it to  
   the local repository
2. Staged- this mean that you have moves the changed file to the index to be commited in the local
   repository
3. Commit- This means that you have stored your changed data safelyt in your local repository.
